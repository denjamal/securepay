﻿using System;

namespace SecurePay.Models
{
    public class TokenisedCard
    {
        public string MerchantCode { get; set; }
        public string Token { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Scheme { get; set; }
        public string Bin { get; set; }
        public string Last4 { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }
}
