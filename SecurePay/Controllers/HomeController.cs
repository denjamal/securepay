﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SecurePay.Models;

namespace SecurePay.Controllers
{
    public class HomeController : Controller
    {
        private const string CLIENT_ID = "0oaxb9i8P9vQdXTsn3l5";
        private const string CLIENT_SECRET = "0aBsGU3x1bc-UIF_vDBA2JzjpCPHjoCP7oI6jisp";
        private const string MERCHANT_CODE = "5AR0055";

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Route("api/process/{amount}")]
        [HttpPost]
        public async Task<IActionResult> ProcessPayment([FromBody]TokenisedCard cardData, double amount)
        {
            var authResponse = await GetAccessTokenAsync();
            var payment = await CreatePayment(authResponse.AccessToken, cardData.Token, amount);
            return Ok(payment);
        }

        private async Task<AuthResponse> GetAccessTokenAsync()
        {
            using (var client = new HttpClient())
            {
                var list = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                    new KeyValuePair<string, string>("scope", "https://api.payments.auspost.com.au/payhive/payments/read https://api.payments.auspost.com.au/payhive/payments/write https://api.payments.auspost.com.au/payhive/payment-instruments/read https://api.payments.auspost.com.au/payhive/payment-instruments/write")
                };
                var content = new FormUrlEncodedContent(list);
                var req = new HttpRequestMessage(HttpMethod.Post, "oauth2/ausujjr7T0v0TTilk3l5/v1/token") { Content = content };
                client.BaseAddress = new Uri("https://hello.sandbox.auspost.com.au");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetBase64($"{CLIENT_ID}:{CLIENT_SECRET}"));
                var response = await client.SendAsync(req);

                if (response.IsSuccessStatusCode)
                {
                    var authResponse = JsonConvert.DeserializeObject<AuthResponse>(await response.Content.ReadAsStringAsync());
                    return authResponse;
                }
                throw new Exception();
            }

        }

        private string GetBase64(string text)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(text);
            var result = Convert.ToBase64String(plainTextBytes);
            return result;
        }

        private async Task<Payment> CreatePayment(string accessKey, string cardToken, double amount)
        {
            using (var client = new HttpClient())
            {
                var testData = new
                {
                    amount = amount,
                    merchantCode = MERCHANT_CODE,
                    token = cardToken,
                    ip = "127.0.0.1",
                };
                var content = new StringContent(JsonConvert.SerializeObject(testData), Encoding.UTF8, "application/json");
                var req = new HttpRequestMessage(HttpMethod.Post, "v2/payments") { Content = content };
                client.BaseAddress = new Uri("https://payments-stest.npe.auspost.zone");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessKey);
                var response = await client.SendAsync(req);

                if (response.IsSuccessStatusCode)
                {
                    var payment = JsonConvert.DeserializeObject<Payment>(await response.Content.ReadAsStringAsync());
                    return payment;
                }
                throw new Exception();
            }
        }
    }
}
